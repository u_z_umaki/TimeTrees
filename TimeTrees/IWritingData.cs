﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json;

namespace TimeTrees
{
    interface IWrite
    {
        void ReadPeople(List<Person> people);
        void ReadTimelineEvent(List<TimeLineEvent> events);
    }
    class WritingDataCsv : IWrite
    {
        public void ReadPeople(List<Person> people)
        {
            string[] dateWrite = new string[people.Count];
            for (int i = 0; i < people.Count; i++)
            {
                string firstParants = people[i].FirstParent == null ? "" : $";{people[i].FirstParent.Id}";
                string lastParants = people[i].SecondParent == null ? "" : $";{people[i].SecondParent.Id}";
                dateWrite[i] = $"{people[i].Id};{people[i].Name};{people[i].DateOfBirth.ToString("yyyy-MM-dd")};{people[i].DateOfBirth.ToString("yyyy-MM-dd")}{firstParants}{lastParants}";             
            }
            File.WriteAllLines("..\\..\\..\\..\\people.csv", dateWrite);
        }
        public void ReadTimelineEvent(List<TimeLineEvent> events)
        {
            string[] dateWrite = new string[events.Count];
            for (int i = 0; i < events.Count; i++)
            {
                dateWrite[i] = $"{events[i].date.ToString("yyyy-MM-dd")};{events[i].events}";
            }
            File.WriteAllLines("..\\..\\..\\..\\timeline.csv", dateWrite);
        }
    }
    class WritingDatajson : IWrite
    {
        public void ReadPeople(List<Person> people)
        {
            
            string dateWrite = JsonConvert.SerializeObject(people);
            File.WriteAllText("..\\..\\..\\..\\people.json", dateWrite);
        }

        public void ReadTimelineEvent(List<TimeLineEvent> events)
        {
            string dateWrite = JsonConvert.SerializeObject(events);
            File.WriteAllText("..\\..\\..\\..\\timeline.json", dateWrite);
        }
    }
}
