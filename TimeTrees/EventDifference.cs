﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class EventDifference
    {
        public void DifferenceDate(List<TimeLineEvent> date)
        {
            DateTime minDATE = date[0].date;
            DateTime maxDATE = date[0].date;
            for (int i = 0; i < date.Count; i++)
            {
                if (minDATE > date[i].date) minDATE = date[i].date;
                if (maxDATE < date[i].date) maxDATE = date[i].date;
            }
            int year = maxDATE.Year - minDATE.Year;
            int month = maxDATE.Month - minDATE.Month;
            int day = maxDATE.Day - minDATE.Day;
            Console.WriteLine($"Лет: {year}, Месяцев: {month}, Дней: {day}");
        }
    }
}
