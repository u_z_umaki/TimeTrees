﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class ShowAllEvents
    {
        public void RenderingListEvents(List<TimeLineEvent> person)
        {
            Console.WriteLine($"DateEvent\tОписание)");
            for (int i = 0; i < person.Count; i++)
            {
                Console.Write($"{person[i].date.ToString("yyyy.MM.dd")}\t");
                Console.WriteLine($"{person[i].events}");
            }
        }
    }
}
