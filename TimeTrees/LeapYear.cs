﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class LeapYear
    {
        private static bool IsLeapYear(int year)
        {
            return year % 400 == 0 || (year % 100 != 0 && year % 4 == 0);
        }
        public void Check(List<Person> lines)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                if ((lines[i].DateOfBirth.Year + 20 > DateTime.Now.Year) && (IsLeapYear(lines[i].DateOfBirth.Year)))
                {
                    Console.WriteLine(lines[i].Name);
                }
            }
        }
    }
}
