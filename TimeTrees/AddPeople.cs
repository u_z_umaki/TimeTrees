﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class AddPeople
    {
        public void Add(List<Person> people)
        {
            ShowAllPeople.RenderingListPeople(people);
            int id = people[people.Count - 1].Id + 1;
            Console.WriteLine("Введите ФИО человека: ");
            string fIO = Console.ReadLine();
            DateTime dateOfBirthADD;
            DateTime? dateOfDeadADD;
            do
            {
                try
                {
                    dateOfBirthADD = DateTime.Parse(DateHelp.AddDate("рождения"));
                    string dataOfDeadString = DateHelp.AddDate("смерти");
                    if (dataOfDeadString == null)
                    {
                        dateOfDeadADD = null;
                    }
                    else
                    {
                        dateOfDeadADD = DateTime.Parse(dataOfDeadString);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Вы ввели не корректную дату");
                    continue;
                }

                if (CheckBirthAndDeadDate(dateOfBirthADD, dateOfDeadADD) == true)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Не может быть дата смерти меньше даты рождения!");
                    continue;
                }
            } while (true);
            Console.WriteLine("Введите ID первого родителя");
            int idFirstParent = AddID(people);
            Console.WriteLine("Введите ID второго родителя");
            int idSecondParent = AddID(people);
            people.Add(new Person 
            { 
                Id = id, 
                Name = fIO, 
                DateOfBirth = dateOfBirthADD, 
                DateOfDead = dateOfDeadADD, 
                FirstParent = idFirstParent == 0?null : people[idFirstParent - 1],
                SecondParent = idSecondParent == 0 ? null : people[idSecondParent - 1]
            });
        }
        private bool CheckBirthAndDeadDate(DateTime BirthDate, DateTime? DeadDate)
        {
            if (DeadDate != null)
            {
                if (((DateTime)DeadDate - BirthDate).TotalDays < 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        private int AddID(List<Person> person)
        {
            do
            {
                string ID = Console.ReadLine();
                if (ID == "")
                {
                    return 0;
                }
                int id = int.Parse(ID);
                if (CheckId(id, person) != true)
                {
                    Console.WriteLine("Человека с таким Id не существует! Попробуйте ещё раз.");
                }
                else
                {
                    return id;
                }
            } while (true);
        }
        private bool CheckId(int id, List<Person> person)
        {
            bool checkId = false;
            for (int i = 0; i < person.Count; i++)
            {
                checkId = person[i].Id == id;
                if (checkId == true)
                {
                    break;
                }
            }
            return checkId;
        }
    }
}
