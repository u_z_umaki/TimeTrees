﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    static class DateHelp
    {
        static public DateTime ParseDate(string stDate)
        {
            var parseString = stDate;
            if (parseString.Length == 4) return DateTime.ParseExact(parseString, "yyyy", null);
            else if (parseString.Length == 7) return DateTime.ParseExact(parseString, "yyyy-MM", null);
            else return DateTime.ParseExact(parseString, "yyyy-MM-dd", null);
        }
        //static public DateTime AddDate(string DateOfWhat)
        //{
        //    string[] dateEvent = new string[3];
        //    Console.Write($"Введите год {DateOfWhat}: ");
        //    dateEvent[0] = Console.ReadLine();
        //    Console.Write($"Введите месяц {DateOfWhat}: ");
        //    dateEvent[1] = Console.ReadLine();
        //    if (dateEvent[1] == "")dateEvent[1] = "01";
        //    Console.Write($"Введите день {DateOfWhat}: ");
        //    dateEvent[2] = Console.ReadLine();
        //    if (dateEvent[2] == "") dateEvent[2] = "01";
        //    return DateTime.Parse(string.Join('-', dateEvent));
        //}
        static public string /*DateTime?*/ AddDate(string DateOfWhat/*, int NULLVALUE = 0*/)
        {
            string[] dateEvent = new string[3];
            Console.Write($"Введите год {DateOfWhat}: ");
            dateEvent[0] = Console.ReadLine();
            if (dateEvent[0] == "")
            {
                return null;
            }
            Console.Write($"Введите месяц {DateOfWhat}: ");
            dateEvent[1] = Console.ReadLine();
            if (dateEvent[1] == "") dateEvent[1] = "01";
            Console.Write($"Введите день {DateOfWhat}: ");
            dateEvent[2] = Console.ReadLine();
            if (dateEvent[2] == "") dateEvent[2] = "01";
            return /*DateTime.Parse(*/string.Join('-', dateEvent)/*)*/;
        }
    }
}
