﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace TimeTrees
{    
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>();
            List<TimeLineEvent> timeLineEvents = new List<TimeLineEvent>();
            ReadData readData = new ReadData();
            readData.Read(ref people, ref timeLineEvents);
            string[] options = { "Leap year", "EventDifference", "Add Event", "Add People", "Show all people", "Show all Events", "Exit" };
            string contents = "Меню";
            do
            {
                Console.Clear();
                IMenu menu = new MenuRendering(options, contents);
                int selectedIndex = menu.Run();
                switch (selectedIndex)
                {
                    case 0:
                        Console.Clear();
                        LeapYear leapYear = new LeapYear();
                        leapYear.Check(people);
                        Console.ReadKey(true);
                        break;
                    case 1:
                        Console.Clear();
                        EventDifference eventDifference = new EventDifference();
                        eventDifference.DifferenceDate(timeLineEvents);
                        Console.ReadLine();
                        break;
                    case 2:
                        Console.Clear();
                        AddEvent addEvent = new AddEvent();
                        addEvent.Add(timeLineEvents, people);
                        break;
                    case 3:
                        Console.Clear();
                        AddPeople addPeople = new AddPeople();
                        addPeople.Add(people);                        
                        break;
                    case 4:
                        Console.Clear();
                        ShowAllPeople.RenderingListPeople(people);
                        Console.ReadLine();
                        break;
                    case 5:
                        Console.Clear();
                        ShowAllEvents showAllEvents = new ShowAllEvents();
                        showAllEvents.RenderingListEvents(timeLineEvents);
                        Console.ReadLine();
                        break;
                    case 6:
                        IWrite writeCsv = new WritingDataCsv();
                        writeCsv.ReadPeople(people);
                        writeCsv.ReadTimelineEvent(timeLineEvents);
                        IWrite writeJson = new WritingDatajson();
                        writeJson.ReadPeople(people);
                        writeJson.ReadTimelineEvent(timeLineEvents);
                        Console.Clear();                       
                        Environment.Exit(0);
                        break;
                }
            } while (true);
        }
    }   
}
